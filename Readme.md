# RPN Calculator
A tool which implements a simple Reverse Polish Notation Calculator

## Author: Mark Williamson

### Requirements

* Java 8 or better
* Gradle 5.x or better

### Building
``
gradle build uberJar
``

This builds a fat jar containing all dependencies. Only the jar file is required to run the application.

###Running

```
 java -jar build/libs/rpn-1.0.jar 
``` 

### Using

A simple example
```
RPN Calculator (Ctrl+C to exit)
$
3<enter>
stack: 3 
$
4<enter>
stack: 3 4 
$
+<enter>
stack: 7 
$

```

##Operators

| Operator | Meaning               |
|--------|-----------------------|
| \- | Subtraction           |
|  \+ | Addition              |
|  \* | Multiplication        |
|  / | Division              |
| sqrt | Square root           |
| undo | Remove last operation |
|  clear | Clear the stack       |


package org.ohmslaw.rpn;

public class CalculatorException extends RuntimeException {

    public CalculatorException(String message) {
        super(message);
    }
}

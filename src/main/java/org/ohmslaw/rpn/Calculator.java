package org.ohmslaw.rpn;


import org.ohmslaw.rpn.operator.Operator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

class Calculator {

    private final Stack<Operator> stack ;
    private final Stack<Operator> undoStack;
    private final BufferedReader inputReader;

    Calculator(InputStream in, Stack<Operator> stack, Stack<Operator> undoStack  ) {
        inputReader = new BufferedReader(new InputStreamReader(in));
        this.stack = stack;
        this.undoStack = undoStack;
    }

    public void process() throws IOException {

        System.out.println("$");
        String input = inputReader.readLine();
        String[] elements = input.split(" ");
        AtomicInteger position = new AtomicInteger();
        for (String element : elements) {
            Operator operator = Operator.of(element);
            position.getAndIncrement();
            try {
                operator.process(stack, undoStack);
            } catch (CalculatorException ex) {
                System.out.println(formatErrorMessage(position, operator, ex.getMessage()));
                break;
            }
        }
        printStack(stack);
    }

    private String formatErrorMessage(AtomicInteger pos, Operator op, String message) {
        return "Operator " + op.formatOutput() + " (position: " + (pos.get() + (pos.get() - 1)) + "):" +  message;
    }

    private void printStack(Stack<Operator> stack) {
        System.out.print("stack: ");
        stack.forEach(bd -> {
            System.out.print(bd.formatOutput());
            System.out.print(" ");
            }

        );
        System.out.println();
    }


}

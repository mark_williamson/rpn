package org.ohmslaw.rpn.operator;

import org.ohmslaw.rpn.CalculatorException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

public class DivideOperator implements Operator {

    @Override
    public void process(Stack<Operator> stack, Stack<Operator> undoStack) {
        recordUndoOperation(stack,undoStack);
        Operator first = stack.pop();
        Operator second = stack.pop();
        if (first.getNumber().doubleValue() == 0) {
            throw new CalculatorException("Can't divide by zero");
        }
        stack.push(new NumberOperator(second.getNumber().divide(first.getNumber(), RoundingMode.HALF_UP )));
    }

    @Override
    public String formatOutput() {
        return "/";
    }
}

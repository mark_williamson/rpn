package org.ohmslaw.rpn.operator;

import org.ohmslaw.rpn.CalculatorException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

public interface Operator {

    int CACLULATOR_PRECISION = 15;

    static Operator of(String element) {
        switch (element) {
            case "+":
                return new AddOperator();
            case "-":
                return new SubtractOperator();
            case "*":
                return new MultiplyOperator();
            case "/":
                return new DivideOperator();
            case "sqrt":
                return new SquareRootOperator();
            case "clear":
                return new ClearOperator();
            case "undo":
                return new UndoOperator();
            default:
                try {
                    BigDecimal num = new BigDecimal(element).setScale(CACLULATOR_PRECISION, RoundingMode.HALF_UP);
                    return new NumberOperator(num);
                } catch (Exception e) {
                    throw new CalculatorException("Not a valid number or operator");
                }
        }
    }

    void process(Stack<Operator> stack, Stack<Operator> undoStack);

    default String formatOutput() {
        return "";
    }

    default BigDecimal getNumber() {
        throw new CalculatorException("Not valid on an operator");
    }

    default void recordUndoOperation(Stack<Operator> stack, Stack<Operator> undoStack) {
        if (stack.size()<2 ) {
            throw new CalculatorException("insufficient parameters");
        }
        undoStack.push(this);


    }


}

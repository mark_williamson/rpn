package org.ohmslaw.rpn.operator;

import java.util.Stack;

public class ClearOperator implements Operator {

    @Override
    public void process(Stack<Operator> stack, Stack<Operator> undoStack) {
        stack.clear();
        undoStack.clear();
    }

}

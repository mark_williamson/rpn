package org.ohmslaw.rpn.operator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Stack;

public class NumberOperator implements Operator {

    private static final int DISPLAY_MAX_DIGITS = 10;
    private static final int DISPLAY_MIN_DIGITS = 0;
    private final BigDecimal number;

    NumberOperator(BigDecimal num) {
        this.number = num;
    }

    @Override
    public void process(Stack<Operator> stack, Stack<Operator> undoStack) {
       stack.push(this);
       undoStack.push(this);
    }

    @Override
    public BigDecimal getNumber() {
        return number;
    }


    public String formatOutput() {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(DISPLAY_MAX_DIGITS);
        df.setMinimumFractionDigits(DISPLAY_MIN_DIGITS);
        df.setGroupingUsed(false);
        return  df.format(number);
    }
}

package org.ohmslaw.rpn.operator;

import org.ohmslaw.rpn.CalculatorException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class UndoOperator implements Operator {
    @Override
    public void process(Stack<Operator> stack, Stack<Operator> undoStack) {
        if (undoStack.isEmpty()) {
          throw new CalculatorException("insufficient parameters");
        }
        undoStack.pop();
        replayUndoStack(stack, undoStack);
    }

    private void replayUndoStack(Stack<Operator> stack, Stack<Operator> undoStack) {
        stack.clear();
        Stack<Operator> tempUndoStack = new Stack<>();
        List<Operator> operators = new ArrayList<>(undoStack);
        Collections.reverse(undoStack);
        operators.forEach(op -> op.process(stack,tempUndoStack));
        Collections.reverse(undoStack);

    }

}

package org.ohmslaw.rpn.operator;

import org.ohmslaw.rpn.CalculatorException;

import java.math.BigDecimal;
import java.util.Stack;



public class SquareRootOperator implements Operator {

    @Override
    public void process(Stack<Operator> stack, Stack<Operator> undoStack) {
        recordUndoOperation(stack,undoStack);
        Operator first = stack.pop();

        stack.push(new NumberOperator(sqrt(first.getNumber())));
    }

    private static BigDecimal sqrt(BigDecimal x) {
        if (x.doubleValue() < 0) {
            throw new CalculatorException("Can't calculate the square root of a negative number");
        }
        return BigDecimal.valueOf(StrictMath.sqrt(x.doubleValue()));
    }

    @Override
    public String formatOutput() {
        return "sqrt";
    }


    public void recordUndoOperation(Stack<Operator> stack, Stack<Operator> undoStack) {
        if (stack.size()<1) {
            throw new CalculatorException("insufficient parameters");
        }
        undoStack.push(this);


    }
}

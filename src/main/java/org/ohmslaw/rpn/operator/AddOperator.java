package org.ohmslaw.rpn.operator;

import java.util.Stack;

public class AddOperator implements Operator {


    @Override
    public void process(Stack<Operator> stack, Stack<Operator> undoStack) {
        recordUndoOperation(stack,undoStack);
        Operator first = stack.pop();
        Operator second = stack.pop();
        stack.push(new NumberOperator(first.getNumber().add(second.getNumber())));
    }

    @Override
    public String formatOutput() {
        return "+";
    }
}

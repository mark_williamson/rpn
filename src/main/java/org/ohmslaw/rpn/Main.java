package org.ohmslaw.rpn;

import org.ohmslaw.rpn.operator.Operator;

import java.io.IOException;
import java.util.Stack;

public class Main {

    public static void main(String[] args) throws IOException {

        Stack<Operator> stack = new Stack<>();
        Stack<Operator> undoStack = new Stack<>();
        Calculator calculator = new Calculator(System.in, stack, undoStack);
        System.out.println("RPN Calculator (Ctrl+C to exit)");
        while (true) {
            try {
                calculator.process();
            } catch (CalculatorException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}

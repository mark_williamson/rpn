package org.ohmslaw.rpn;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ohmslaw.rpn.operator.Operator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Stack;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;

public class CalculatorTest {

    private Stack<Operator> stack;
    private Stack<Operator> undoStack = new Stack<>();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;


    @Before
    public void setup() {
        stack = new Stack<>();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUp() {
        System.setOut(originalOut);
    }

    @Test
    public void testThatASingleNumberIsProcessed() throws IOException {
        String data = "10";
        ByteArrayInputStream input  = new ByteArrayInputStream(data.getBytes());
        Calculator calculator = new Calculator(input,stack,undoStack);
        calculator.process();

        assertThat(stack,hasSize(1));
    }

    @Test
    public void testThatASuitableMessageIsDisplayedWhenInsufficientParams() throws IOException {
        String data = "*";
        ByteArrayInputStream input  = new ByteArrayInputStream(data.getBytes());
        Calculator calculator = new Calculator(input,stack,undoStack);



        String expected =  "$\n" +
                "Operator * (position: 1):insufficient parameters\n" +
                "stack: \n";

        calculator.process();
        assertThat(outContent.toString(), is(expected));

    }

    @Test
    public void testThatAlOperatorsCombinedTogetherWork() throws IOException {
        String data = "2 4 5 6 7 8.123 + * - / / undo -1 * sqrt";
        ByteArrayInputStream input  = new ByteArrayInputStream(data.getBytes());
        Calculator calculator = new Calculator(input,stack,undoStack);



        String expected =  "$\n" +
                "stack: 2 0.2159948127 " +
                "\n";

        calculator.process();
        assertThat(outContent.toString(), is(expected));

    }

    @Test
    public void testThatProcessingHaltsWithInsufficientParams() throws IOException {
        String data = "1 2 3 * 5 + * * 6 5";
        ByteArrayInputStream input  = new ByteArrayInputStream(data.getBytes());
        Calculator calculator = new Calculator(input,stack,undoStack);



        String expected =  "$\n" +
                "Operator * (position: 15):insufficient parameters\n" +
                "stack: 11 \n";

        calculator.process();
        assertThat(outContent.toString(), is(expected));

    }

}

package org.ohmslaw.rpn.operator;

import org.junit.Before;
import org.junit.Test;

import java.util.Stack;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class NumberOperatorTest {

    private Stack<Operator> stack;
    private Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
    }

    @Test
    public void testThatNumbersAreStoredToFifteenDigits() {

        stack.push(Operator.of("0.00000000002345"));
        stack.push(Operator.of("10000000000"));

        MultiplyOperator addOperator = new MultiplyOperator();
        addOperator.process(stack,undoStack);
        Operator numberOperator = stack.pop();

        assertThat(numberOperator.formatOutput(), is("0.2345"));
    }



    @Test
    public void testThatOutputTruncatesNumbersWherePossible() {

        Operator numberOperator = Operator.of("10");
        assertThat(numberOperator.formatOutput(), is("10"));
    }

    @Test
    public void testThatOutputDisplaysTenPlaces() {
        Operator numberOperator = Operator.of("10.012345678912345");
        assertThat(numberOperator.formatOutput(), is("10.0123456789"));
    }
}

package org.ohmslaw.rpn.operator;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.ohmslaw.rpn.CalculatorException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

public class UndoOperatorTest {


    private Stack<Operator> stack;
    private Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
        stack.push(Operator.of("6"));
        stack.push(Operator.of("4"));

        undoStack.push(Operator.of("6"));
        undoStack.push(Operator.of("4"));
    }

    @Test
    public void testTwoParameterOperatorUndo() {


        Operator operator = Operator.of("+");
        operator.process(stack,undoStack);
        Operator undo = Operator.of("undo");
        undo.process(stack,undoStack);

        assertStack(stack,6,4);
        assertStack(undoStack,6,4);

    }

    @Test
    public void testASingleNumber() {

        Operator undo = Operator.of("undo");
        undo.process(stack,undoStack);

        assertStack(stack,6);
        assertStack(undoStack,6);

    }

    @Test
    public void testOneParameterOperationUndo() {

        Operator operator = Operator.of("sqrt");
        operator.process(stack,undoStack);
        Operator undo = Operator.of("undo");
        undo.process(stack,undoStack);

        assertStack(stack,6,4);
        assertStack(undoStack,6,4);

    }

    @Test
    public void testMultipleOperatorsAndUndos() {
        stack.clear();
        undoStack.clear();
        stack.push(Operator.of("13"));

        undoStack.push(Operator.of("6"));
        undoStack.push(Operator.of("4"));
        undoStack.push(Operator.of("3"));
        undoStack.push(Operator.of("+"));
        undoStack.push(Operator.of("+"));

        Operator undo = Operator.of("undo");
        undo.process(stack,undoStack);
        Operator undo2 = Operator.of("undo");
        undo2.process(stack,undoStack);

        assertStack(stack,6,4,3);
        assertStack(undoStack,6,4,3);
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testThatAnExceptionIsThrownWhenUndoCalledWithNoParams() {
        stack.clear();
        undoStack.clear();
        exceptionRule.expect(CalculatorException.class);
        exceptionRule.expectMessage("insufficient parameters");


        Operator undo = Operator.of("undo");
        undo.process(stack,undoStack);

    }

    private void assertStack(Stack<Operator> stack, Integer ... values) {
        assertThat(stack,hasSize(values.length));
        List<Integer> valueList = Arrays.asList(values);
        Collections.reverse(valueList);
        for(int value : values) {
            assertThat(stack.pop().getNumber(), closeTo(new BigDecimal(value),new BigDecimal("0.001")));
       }

    }

}



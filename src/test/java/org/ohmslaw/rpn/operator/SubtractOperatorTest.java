package org.ohmslaw.rpn.operator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

public class SubtractOperatorTest {


    private Stack<Operator> stack;
    private Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
    }

    @Test
    public void testThatSubtractingProducesExpectedResult() {
        stack.push(Operator.of("10"));
        stack.push(Operator.of("4"));

        SubtractOperator operator = new SubtractOperator();

        operator.process(stack,undoStack);
        Operator result = stack.pop();

        assertThat(result.getNumber(), closeTo(new BigDecimal(6),new BigDecimal("0.001")));

        assertThat(undoStack,hasSize(1));
        Assert.assertThat(undoStack.pop(),instanceOf(SubtractOperator.class));

    }

    @Test
    public void formatOutput() {

        SubtractOperator operator = new SubtractOperator();
        assertThat(operator.formatOutput(), is("-"));
    }
}

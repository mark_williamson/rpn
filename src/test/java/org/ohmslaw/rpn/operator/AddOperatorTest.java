package org.ohmslaw.rpn.operator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

public class AddOperatorTest {

    private Stack<Operator> stack;
    private Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
    }

    @Test
    public void testThatAddingProducesExpectedResult() {
           stack.push(Operator.of("2"));
           stack.push(Operator.of("4"));

           AddOperator addOperator = new AddOperator();

           addOperator.process(stack,undoStack);
           Operator result = stack.pop();

           assertThat(result.getNumber(), closeTo(new BigDecimal(6),new BigDecimal("0.001")));

        assertThat(undoStack,hasSize(1));
        Assert.assertThat(undoStack.pop(),instanceOf(AddOperator.class));

    }

    @Test
    public void formatOutput() {

        AddOperator addOperator = new AddOperator();
        assertThat(addOperator.formatOutput(), is("+"));
    }
}

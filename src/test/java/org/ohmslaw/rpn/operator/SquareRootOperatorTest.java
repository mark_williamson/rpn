package org.ohmslaw.rpn.operator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.ohmslaw.rpn.CalculatorException;

import java.math.BigDecimal;
import java.util.Stack;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

public class SquareRootOperatorTest {

    private Stack<Operator> stack;
    private Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
    }

    @Test
    public void testThatSquareRootProducesExpectedResult() {
        stack.push(Operator.of("9"));


        SquareRootOperator operator = new SquareRootOperator();

        operator.process(stack,undoStack);
        Operator result = stack.pop();

        assertThat(result.getNumber(), closeTo(new BigDecimal(3),new BigDecimal("0.001")));

        assertThat(undoStack,hasSize(1));
        Assert.assertThat(undoStack.pop(),instanceOf(SquareRootOperator.class));

    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testThatSquareRootHandlesNegativeNumbers() {
        stack.push(Operator.of("-5"));
        SquareRootOperator operator = new SquareRootOperator();

        exceptionRule.expect(CalculatorException.class);
        exceptionRule.expectMessage("Can't calculate the square root of a negative number");
        operator.process(stack,undoStack);
    }

    @Test
    public void formatOutput() {

        SquareRootOperator operator = new SquareRootOperator();
        assertThat(operator.formatOutput(), is("sqrt"));
    }

    @Test
    public void testThatTooFewOperatorsOnStackThrowsException() {
        exceptionRule.expect(CalculatorException.class);
        exceptionRule.expectMessage("insufficient parameters");
        SquareRootOperator operator = new SquareRootOperator();

        operator.process(stack,undoStack);
    }
}

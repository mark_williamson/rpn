package org.ohmslaw.rpn.operator;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Stack;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

public class MultiplyOperatorTest {


    private Stack<Operator> stack;
    private Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
    }

    @Test
    public void testThatMultiplicationProducesExpectedResult() {
        stack.push(Operator.of("10"));
        stack.push(Operator.of("4"));

        MultiplyOperator operator = new MultiplyOperator();

        operator.process(stack,undoStack);
        Operator result = stack.pop();

        assertThat(result.getNumber(), closeTo(new BigDecimal(40),new BigDecimal("0.001")));

        assertThat(undoStack,hasSize(1));
        Assert.assertThat(undoStack.pop(),instanceOf(MultiplyOperator.class));

    }

    @Test
    public void formatOutput() {

        MultiplyOperator operator = new MultiplyOperator();
        assertThat(operator.formatOutput(), is("*"));
    }
}



package org.ohmslaw.rpn.operator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.ohmslaw.rpn.CalculatorException;

import java.math.BigDecimal;
import java.util.Stack;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

public class DivideOperatorTest {


    private  Stack<Operator> stack;
    private  Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
    }

    @Test
    public void testThatDividingProducesExpectedResult() {
        stack.push(Operator.of("10"));
        stack.push(Operator.of("4"));

        DivideOperator operator = new DivideOperator();

        operator.process(stack,undoStack);
        Operator result = stack.pop();

        assertThat(result.getNumber(), closeTo(new BigDecimal("2.5"),new BigDecimal("0.001")));

        assertThat(undoStack,hasSize(1));
        Assert.assertThat(undoStack.pop(),instanceOf(DivideOperator.class));

    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testThatDivideHandlesDivideByZero() {
        stack.push(Operator.of("5"));
        stack.push(Operator.of("0"));
        DivideOperator operator = new DivideOperator();

        exceptionRule.expect(CalculatorException.class);
        exceptionRule.expectMessage("Can't divide by zero");
        operator.process(stack,undoStack);
    }

    @Test
    public void formatOutput() {

        DivideOperator operator = new DivideOperator();
        assertThat(operator.formatOutput(), is("/"));
    }
}


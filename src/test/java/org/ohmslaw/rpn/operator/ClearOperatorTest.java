package org.ohmslaw.rpn.operator;

import org.junit.Before;
import org.junit.Test;

import java.util.Stack;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class ClearOperatorTest {

    private  Stack<Operator> stack;
    private  Stack<Operator> undoStack;

    @Before
    public void setup() {
        stack = new Stack<>();
        undoStack = new Stack<>();
    }

    @Test
    public void testThatClearProducesExpectedResult() {
        stack.push(Operator.of("2"));
        stack.push(Operator.of("4"));

        undoStack.push(Operator.of("2"));
        undoStack.push(Operator.of("4"));

        ClearOperator operator = new ClearOperator();

        operator.process(stack,undoStack);


        assertThat(stack,hasSize(0));
        assertThat(undoStack,hasSize(0));


    }


}


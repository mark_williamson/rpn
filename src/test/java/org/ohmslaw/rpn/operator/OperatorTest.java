package org.ohmslaw.rpn.operator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.ohmslaw.rpn.CalculatorException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class OperatorTest {

    @Test
    public void testThatAddOperatorIsCreated() {

        assertThat(Operator.of("+"),instanceOf(AddOperator.class));
    }

    @Test
    public void testThatSubtractOperatorIsCreated() {

        assertThat(Operator.of("-"),instanceOf(SubtractOperator.class));
    }

    @Test
    public void testThatMultiplyOperatorIsCreated() {

        assertThat(Operator.of("*"),instanceOf(MultiplyOperator.class));
    }

    @Test
    public void testThatDivideOperatorIsCreated() {

        assertThat(Operator.of("/"),instanceOf(DivideOperator.class));
    }

    @Test
    public void testThatClearOperatorIsCreated() {

        assertThat(Operator.of("clear"),instanceOf(ClearOperator.class));
    }

    @Test
    public void testThatUndoOperatorIsCreated() {

        assertThat(Operator.of("undo"),instanceOf(UndoOperator.class));
    }

    @Test
    public void testThatNumberOperatorIsCreated() {

        assertThat(Operator.of("20"),instanceOf(NumberOperator.class));
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Test
    public void testThatUnknownOperatorThrowsException() {
        exceptionRule.expect(CalculatorException.class);
        exceptionRule.expectMessage("Not a valid number or operator");
        assertThat(Operator.of("not"),instanceOf(NumberOperator.class));
    }

    @Test
    public void testThatSquareRootOperatorIsCreated() {

        assertThat(Operator.of("sqrt"),instanceOf(SquareRootOperator.class));
    }

    @Test
    public void testThatFormatReturnsAnEmptyString() {
        Operator op = Mockito.mock(Operator.class);
        when(op.formatOutput()).thenCallRealMethod();
        assertThat(op.formatOutput(),is(""));
    }

    @Test
    public void testThatGetNumberThrowsAnException() {
        Operator op = Mockito.mock(Operator.class);
        when(op.getNumber()).thenCallRealMethod();
        exceptionRule.expect(CalculatorException.class);
        exceptionRule.expectMessage("Not valid on an operator");
        op.getNumber();
    }



}
